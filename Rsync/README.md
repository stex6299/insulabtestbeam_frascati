# What's here

Qui dentro ci sono due script [script.raw.sh](./script.raw.sh) e [script.ascii.sh](./script.ascii.sh) che hanno il compito di sincronizzare rispettivamente i files raw e gli ascii strippati dell'acquisizione sulla cartella CERNBOX comune `/eos/project/i/insulab-como`.
Per poter leggere tali dati, per esempio via lxplus, è necessario essere nel CERN e-group `cernbox-project-insulab-como-readers`: chiedete a [Stefano](mailto:scarsi@studenti.uninsubria.it) per ulteriori informazioni

Le cartelle possono essere montate manualmente in sola lettura con l'opzione
```bash
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 ./ascii_daq_sshfs/
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/datadir_klever22 ./raw_daq_sshfs/
```
oppure usando lo script [setup.sh](./setup.sh)



# How to use
Gli script possono essere chiamati singolarmente, generalmente in un tmux, come `k5reauth ./script.ascii.sh`
