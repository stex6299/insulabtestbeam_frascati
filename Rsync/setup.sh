#!/bin/bash

asciidir="./ascii_daq_sshfs"
rawdir="./raw_daq_sshfs"

echo "Provo a smontare le cartelle"
fusermount -u $asciidir
fusermount -u $rawdir


if [ ! -d $asciidir ]; then
    echo "Creating $asciidir"
    mkdir $asciidir
fi

if [ ! -d $rawdir ]; then
    echo "Creating $rawdir"
    mkdir $rawdir
fi

echo "Cleaning $asciidir"
rm -vrf $asciidir/*
echo "Cleaning $rawdir"
rm -vrf $rawdir/*


echo "Mounting data folder..."
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 $asciidir/ -o nonempty
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/datadir_klever22 $rawdir/ -o nonempty
echo "Mounted!!"