#!/usr/bin/env python3

"""
Script per convertire in tempo reale i dati in HDF5
Rivisto per BTF Frascati, 2023 04
"""


print("Buongiorno, mi sto avviando")


#%% SETTINGS
configFile = r"./config.json"

# Chiavi del file json, così sono definite in un posto solo
asciiPath_key = "asciiPath"
HDF5_key = "HDF5"
numWaveform_key = "numWaveform"
currAscii_key = "currAscii"
currSpill_key = "currSpill"



#%% LIBRARIES
import numpy as np
import os
import pandas as pd

import json
import re
import time
import sys
import glob

import h5py
from io import StringIO





#%% FUNCTIONS

def elencaRun(asciiFolder):
    """
    RETURN: elencaRun ritorna una lista con tutti i numeri di run esistenti 
    nella cartella asciiFolder
    
    Funzionamento:
    - Mi faccio elencare tutte le prime spill
    - Splitto al separatore (os.path.sep) 
      Per compatibilità con maligno, mi tengo i due separatori a mano
    - Prendo l'ultima cosa (-1), ovvero il nome del file
    - Dal carattere 3 al 9 c'è il numero di run, converto ad intero
    
    CALL: lstofAllRun = elencaRun(settings[asciiPath_key])
    
    TODO: Magari farla in più righe, essendo una funzione, così diventa più leggibile
    
    """
    
    lstofAllRun = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(asciiFolder,"*_00001.dat"))]
    return lstofAllRun


def elencaSpill(asciiFolder, numRun):
    """
    RETURN: elencaSpill ritorna una lista con tutti i numeri di spill esistenti
    nella cartella asciiFolder per la run numRun
    
    CALL: lstofAllSpill = elencaSpill(settings[asciiPath_key], 501230)
    
    FEATURES:
    

    """
        
    lstofAllSpill = [int(re.split(r"\\|/", f)[-1].split("_")[-1].split(".")[0]) for f in glob.glob(os.path.join(asciiFolder,f"run{numRun}_*.dat"))]
    return lstofAllSpill





# Nota: gli ascii pariono iniziare con uno spazio: lo tolgo con lstrip
def scriviDati(HDF5file, ASCIIfile, numWF,):
    """
    TODO:
        - Testare con files vuoti, non so dove potrebbe rompersi, ma credo sia
        a posto
    """
    
    # Controllo che il file non sia vuoto
    if os.path.getsize(ASCIIfile) == 0:
        print(f"Il file {ASCIIfile} è vuoto")
        return
    
            
    
    # Se ho delle waveform, apro il file ascii in questione e lo appiattisco
    with open(ASCIIfile, "r") as f:
        
        # Linee del file appiattito
        newtxt = []
        
        # Ciclo sulle linee
        for i,line in enumerate(f):
            
            if line in ["", "\n"]: continue
            
            # Se sono un multiplo di numWF+1, allora ho finito un blocco 
            if i % (numWF+1) == 0:
                # Se non sono al primo giro, mi appendo la linea precedente
                # con l'eccezione dell'ultimo spazio
                if i!=0:
                    newtxt.append(tmpLine[:-1] + "\n")
                # Replace 4, 3 and 2 spaces into 1
                tmpLine = line.lstrip().replace("\n", " ").replace("    ", " ").replace("   ", " ").replace("  ", " ")
            else:
                tmpLine = tmpLine + line.lstrip().replace("\n", " ").replace("    ", " ").replace("   ", " ").replace("  ", " ")
        
        # Appendo l'ultima riga
        if i>0:
            newtxt.append(tmpLine[:-1] + "\n")
        
        
        testoAppiattito = "".join(newtxt)
        csvStringIO = StringIO(testoAppiattito)
        
        # Dovrebbe essere superfluo
        if len(testoAppiattito) == 0: return
        
        df = pd.read_csv(csvStringIO, sep=" ", header=None, skiprows=0, )
            
    
    
    
    righeLastFile = len(df.index)
    print(f"Numero di colonne: {len(df.columns)}")
    print(f"Numero di righe: {righeLastFile}")

    
    # Scrivo i dati nel file HDF5
    # Posso aprirlo in append anche se non esiste
    with h5py.File(HDF5file, 'a', libver='latest') as hf:
        print(HDF5file)
    
        hf.swmr_mode = True
		
		
        # Se il file non esisteva
        opts = {"compression":"gzip", "chunks":True}
        
        if (len(hf.keys())) == 0:
            
            
            # Frascati 2023 Settings
            hf.create_dataset("xpos", data =  df.iloc[:,0:4], maxshape=(None,4), **opts)
            hf.create_dataset("digi_ph", data =  df.iloc[:,4:20], maxshape=(None,16), **opts)
            hf.create_dataset("digi_time", data =  df.iloc[:,20:36], maxshape=(None,16), **opts)
            
            hf.create_dataset("itime", data =  df.iloc[:,36], maxshape=(None,), **opts)
            hf.create_dataset("iatime", data =  df.iloc[:,37], maxshape=(None,), **opts)
            hf.create_dataset("ievent", data =  df.iloc[:,38],  maxshape=(None,), **opts)


            """
            #Waveform - Può essere comodo avere qui
            nSamples = 1031
            nStart = 68
            for i in range(numWF):
                hf.create_dataset(f"wf{i}", data =  df.iloc[:,(nStart+nSamples*i):(nStart+nSamples*(i+1))], maxshape=(None,nSamples), **opts)
            """
            
        else:
            
            # ================================================================
            # Provo a capire quant'è la minima shape
            # A volte crasha a metà e, nel crashare, aggiorna solamente alcune colonne
            # Andrà inserito nello script principale....

            tmpMin = np.nan

            for k in hf.keys():
                if np.isnan(tmpMin):
                    tmpMin = hf[k].shape[0]
                else:
                    if hf[k].shape[0] < tmpMin:
                        tmpMin = hf[k].shape[0]

            for k in hf.keys():
                #hf[k].resize((hf[k].shape[0] + righeLastFile), axis = 0)
                hf[k].resize((tmpMin + righeLastFile), axis = 0)

            # In questo modo dovrebbe essere robusto
            # =======================================================
                
               
            
            # Frascati 2023 Settings
            hf["xpos"][-righeLastFile:] = df.iloc[:,0:4]
            hf["digi_ph"][-righeLastFile:] = df.iloc[:,4:20]
            hf["digi_time"][-righeLastFile:] = df.iloc[:,20:36]
            
            hf["itime"][-righeLastFile:] = df.iloc[:,36]
            hf["iatime"][-righeLastFile:] = df.iloc[:,37]
            hf["ievent"][-righeLastFile:] = df.iloc[:,38]

            """
            #Waveform
            nSamples = 1031
            nStart = 68
            for i in range(numWF):
                hf[f"wf{i}"][-righeLastFile:] = df.iloc[:,(nStart+nSamples*i):(nStart+nSamples*(i+1))]
            """
            

    return





def aggiornaConfig(settings):
    """
    aggiornaConfig permette di aggiornare il dizionario delle configurazioni.
    Chiamare passando il dizionario stesso. Il file viene invece automaticamente
    pescato dalla funzione.
    
    CALL: aggiornaConfig(settings)
    """
    # Lo salvo
    try:
        with open(configFile, "w") as f:
            json.dump(settings, f, indent=4)
    except Exception as e: 
        print(e)
        print("Non ho salvato le informazioni nel dizionario, resteranno solo in memoria")
        
    return





            




#%% PRELIMINARY CHECK + load config

# Controllo che esista il file di config
try:
    with open(configFile, "r") as f:
        settings = json.load(f)
except:
    print("Sicuro che esista il file di config?")
    sys.exit(1)
    
    
# Check if folder exist
if not os.path.isdir(settings[asciiPath_key]):
    print(f"Ascii path not valid\t{settings[asciiPath_key]}")
    sys.exit(1)
    
if not os.path.isdir(settings[HDF5_key]):
    print(f"HDF5 path not valid\t{settings[HDF5_key]}")
    sys.exit(1)
    
    
    
    
#%% Big loop

# Dopo 10 tentativi, riavvio
contaErrori = 0

while(True):
    
    # Ottengo la lista di tutte le run esistenti
    lstofAllRun = elencaRun(settings[asciiPath_key])
    
    # Controllo che esista nella directory almeno una run, per consistenza
    # con i pezzi seguenti
    if len(lstofAllRun) == 0:
        time.sleep(10)
        continue
        # wait till there exist at least one ascii file


    # Ottengo la lista delle spill dell'ASCII corrente
    lstofAllSpill = elencaSpill(settings[asciiPath_key], settings[currAscii_key])
    
    
    # Se inizializzo bene non dovrebbe essere vitale, ma costa poca fatica lasciarlo
    # e poi è più robusto
    
    # Se l'ASCII non esiste (e quindi non ha spill, verosimilmente è un pede),
    # passo all'ASCII dopo
    if len(lstofAllSpill) == 0: 
        settings[currAscii_key] += 1
        settings[currSpill_key] = 0
        aggiornaConfig(settings)
        continue
    
    
    # Se esiste una nuova spill dell'ASCII corrente
    # La scrivo ed incremento il contatore delle spill
    # Se ho un errore in scrittura, semplicemente passo oltre
    if np.array(lstofAllSpill).max() > settings[currSpill_key]:
        try:
            infile = os.path.join(settings[asciiPath_key], f"run{settings[currAscii_key]}_ascii_{settings[currSpill_key]+1:05d}.dat")
            outfile = os.path.join(settings[HDF5_key], f"run{settings[currAscii_key]}.h5")
                        
            print(f"\nSto per appendere {infile} a {outfile}")
            scriviDati(outfile, infile, settings[numWaveform_key])
            
            settings[currSpill_key] += 1
            aggiornaConfig(settings)
            
            contaErrori = 0
            
            continue
        
        except Exception as e: 
            print(e)
            print("Mannaggia, ci riproviamo al prossimo giro")
            
            contaErrori +=1
            print(f"Errore numero {contaErrori}")
            
            time.sleep(1)
            
            
            """
            Non sono sicuro di volerla sta roba
            if contaErrori >= 10:
                print("Ciao ciao, mi resetto")
                os.system("python3 HDF5.py")
                sys.exit(2)
            #continue
            """
        
    # Se non ho nuove spill, ma in compenso esiste un nuovo ascii        
    elif np.array(lstofAllRun).max() > settings[currAscii_key]:
        
        # NOTA PER IL ME DEL FUTURO:
        # In questo preciso punto ci si arriva solo quando ho finito di appendere
        # i dati dell'ultima spill e mi accorgo che esiste la run successiva.
        # è il punto ideale per creare in un colpo solo tree, npz o formati 
        # compressi non appendibili
        # settings[currAscii_key] è il numero della run che mi interessa


        
        # Incremento il contatore
        settings[currAscii_key] += 1
        
        # Incremento finche non esiste la run
        while not(settings[currAscii_key] in lstofAllRun):
            settings[currAscii_key] += 1

        # Imposto la spill a 0 ed incremento il contatore
        settings[currSpill_key] = 0
        aggiornaConfig(settings)
        
        
        


    time.sleep(10)




