#!/bin/bash

sshfsdir="./ASCII_SSHFS"

echo "Provo a smontare la cartella"
fusermount -u $sshfsdir

if [ ! -d $sshfsdir ]; then
    echo "Creating $sshfsdir"
    mkdir $sshfsdir
fi

echo "Cleaning $sshfsdir"
rm -vrf $sshfsdir/*

echo "Mounting data folder..."
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_h2_storm $sshfsdir/ -o nonempty
echo "Mounted!!"