# What's here

In questa cartella si trova lo [script principale](./HDF5.py), che processa i dati. In genere lo chiamo in un tmux come `k5reauth ./HDF5.py`. Questo script è stato derivato dal [repo della scorsa estate](https://gitlab.com/stex6299/insulabtestbeamV2/-/tree/master/data_process_scripts/HDF5_2022), pertanto può contenere qualche bug. Questo repo è temporaneo, non ha la pretesa di diventare definitivo

- [config.json](./config.json) è il file che contiene le configurazioni su numero di run/spill e la cartella di destinazione
- [ASCII_SSHFS](./ASCII_SSHFS) è la cartella in cui vengono montati via sshfs in modalità sola lettura i dati dal pc dell'acquisizione. Può essere fatto manualmente con
```
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_h2_storm ./ASCII_SSHFS/
```
oppure usando lo script [setup.sh](./setup.sh)





## Example of reading such a data
See an example [here](https://scarsi.web.cern.ch/MISC/Python/proveH5.html). Shortly:
```python3
import h5py

with h5py.File('data.h5', 'r', libver='latest', swmr=True) as hf:
	print(hf.keys())
	hf["sili"].shape
```
PLEASE, NOTE TO SPECIFY `swmr=True)` otherwise, if you don't close the file, it become impossible for me to append data

