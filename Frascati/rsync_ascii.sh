#!/bin/bash

sourcedir="./TMPSSHFS"
targetdir="scarsi@lxplus.cern.ch:/eos/project/i/insulab-como/testBeam/TB_BTF_2023_04/ASCII_MICHELA"

echo "I will synchronize " $sourcedir " with " $targetdir


rsync -avrz $sourcedir $targetdir
