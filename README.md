# What's here

Repository temporaneo con gli script di processing dati, per il test beam a Frascati, Beam Test Facility, Aprile 2023

Questo repository è stato derivato da [quello dell'estate precedente](https://gitlab.com/stex6299/insulabtestbeamV2) ma non aspira ad essere quello definitivo.


# What to use
- La cartella [Frascati](./Frascati/) contiene ciò che ho usato sul mio PC per effettuare il sync degli ascii al cern
- Per processare i dati, viene invece usato sulla mia vm [questo script](./data_process_scripts/HDF5.py)